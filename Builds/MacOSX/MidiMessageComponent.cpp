//
//  MidiMessageComponent.cpp
//  JuceBasicWindow
//
//  Created by Tom on 03/11/2016.
//
//

#include "MidiMessageComponent.hpp"

MidiMessageComponent::MidiMessageComponent()
{

    midiLabel.setText ("Midi Label\n", dontSendNotification);
    addAndMakeVisible (midiLabel);
    
    /** Add and apply labels to the GUI*/
    
    messageTypeLabel.setText("Message Type", dontSendNotification);
    addAndMakeVisible(messageTypeLabel);
    
    
    channelLabel.setText("Channel", dontSendNotification);
    addAndMakeVisible(channelLabel);
    
    
    numberLabel.setText("Number", dontSendNotification);
    addAndMakeVisible(numberLabel);
    
    
    velocityLabel.setText("Velocity", dontSendNotification);
    addAndMakeVisible(velocityLabel);
    
    /** Add and apply ComboBox to the GUI*/
    messageTypeBox.addItem("Note On", 1);
    messageTypeBox.addItem("Control", 2);
    messageTypeBox.addItem("Program", 3);
    messageTypeBox.addItem("Pitch Bend", 4);
    messageTypeBox.addItem("Aftertouch", 5);
    messageTypeBox.setSelectedId(1);
    addAndMakeVisible(messageTypeBox);
    
    /** Add and apply sliders to the GUI*/
    
    channelSlider.setSliderStyle(Slider::IncDecButtons);
    channelSlider.setRange(1, 16, 1);
    addAndMakeVisible(channelSlider);
    
    
    numberSlider.setSliderStyle(Slider::IncDecButtons);
    numberSlider.setRange(0, 127, 1);
    addAndMakeVisible(numberSlider);
    
    velocitySlider.setSliderStyle(Slider::IncDecButtons);
    velocitySlider.setRange(0, 127, 1);
    addAndMakeVisible(velocitySlider);
    
}

MidiMessageComponent::~MidiMessageComponent()
{
    
}

void MidiMessageComponent::resized()
{
    midiLabel.setBounds(60, 200, getWidth() - 20, 40);
    messageTypeLabel.setBounds(10, 10, getWidth() - 20, 40);
    channelLabel.setBounds(150, 10, getWidth() - 20, 40);
    numberLabel.setBounds(290, 10, getWidth() - 20, 40);
    velocityLabel.setBounds(410, 10, getWidth() - 20, 40);
    messageTypeBox.setBounds(10, 50, 100, 20);
    channelSlider.setBounds(150, 50, 90, 20);
    numberSlider.setBounds(290, 50, 90, 20);
    velocitySlider.setBounds(410, 50, 90, 20);
    
}

void MidiMessageComponent::handleIncomingMidiMessage(MidiInput* source, const MidiMessage&)
{
    String midiText;
    
    if (message.isNoteOnOrOff())
    {
        midiText << "NoteOn: Channel " << message.getChannel();
        midiText << ":Number" << message.getNoteNumber();
        midiText << ":Velocity" << message.getVelocity();
    }
    
    midiLabel.getTextValue() = midiText;
    deviceManager.getDefaultMidiOutput()->sendMessageNow(message);
    
    DBG("MIDI Message received\n");
}

void MidiMessageComponent::buttonClicked(Button * button)
{
    String midiText;
    
    midiText << "NoteOn: Channel " <<  channelSlider.getValue();
    midiText << ":Number" << numberSlider.getValue();
    midiText << ":Velocity" << velocitySlider.getValue();
    
    midiLabel.getTextValue() = midiText;
    
    message.noteOn(channelSlider.getValue(),numberSlider.getValue(),(UInt8)velocitySlider.getValue());
    
    deviceManager.getDefaultMidiOutput()-> sendMessageNow(message);
    
    DBG("Button clicked");
    
}


