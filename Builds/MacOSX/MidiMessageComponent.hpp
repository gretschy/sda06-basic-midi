//
//  MidiMessageComponent.hpp
//  JuceBasicWindow
//
//  Created by Tom on 03/11/2016.
//
//

#ifndef MidiMessageComponent_hpp
#define MidiMessageComponent_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
/** This class contains all the interface components used to contruct MIDI message*/

class MidiMessageComponent : public Component,
public MidiInputCallback,
public Button::Listener

{
public:
    
    MidiMessageComponent();
    ~MidiMessageComponent();
    
    void resized() override;
    void handleIncomingMidiMessage(MidiInput* source, const MidiMessage&) override;
    void buttonClicked(Button* button) override;
    
private:
    Label midiLabel;
    Label messageTypeLabel;
    Label channelLabel;
    Label numberLabel;
    Label velocityLabel;
    ComboBox messageTypeBox;
    Slider channelSlider;
    Slider numberSlider;
    Slider velocitySlider;
    MidiMessage message;
    AudioDeviceManager deviceManager;

};
#endif /* MidiMessageComponent_hpp */
