/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    deviceManager.setMidiInputEnabled ("USB Axiom 49 Port 1", true);
    deviceManager.addMidiInputCallback (String:: empty, this);
    deviceManager.setDefaultMidiOutput ("SimpleSynth virtual input");
    
    StringArray sa (MidiOutput::getDevices());
    for (String& s : sa)
    {
        DBG(s);
    }
    
    /** Add and apply toggle button to the GUI*/
    sendButton.setButtonText("Send");
    sendButton.setBounds(10, 100, 50, 50);
    addAndMakeVisible(sendButton);
    sendButton.addListener(this);

}

MainComponent::~MainComponent()
{
    deviceManager.removeMidiInputCallback (String::empty, this);
}

void MainComponent::buttonClicked(Button* button)
{
    
}



